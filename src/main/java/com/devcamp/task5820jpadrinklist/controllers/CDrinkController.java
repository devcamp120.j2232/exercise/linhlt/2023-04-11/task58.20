package com.devcamp.task5820jpadrinklist.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5820jpadrinklist.model.CDrink;
import com.devcamp.task5820jpadrinklist.repository.DrinkRepository;

@RestController
@CrossOrigin
public class CDrinkController {
    @Autowired
    DrinkRepository drinkRepository;
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getDrinks(){
        try {
            List<CDrink> drinkList = new ArrayList<CDrink>();
            drinkRepository.findAll().forEach(drinkList::add);
            if (drinkList.size()==0){
                return new ResponseEntity<>(drinkList,HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(drinkList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
